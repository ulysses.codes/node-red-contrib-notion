# node-red-contrib-notion

:warning: very experimental, use at own risk

A set of nodes for interacting with the [Notion API](https://developers.notion.com/).

## Nodes
More complete docs can be found on the nodes.

### notion-db

Queries a db using notion's [database query API](https://developers.notion.com/reference/post-database-query)

### notion-page-blocks

Fetchs a page's content in the form of blocks using the [block children API](https://developers.notion.com/reference/get-block-children)